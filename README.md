# Aplicando segurança em seu CI/CD com Gitlab

Repositório para armazenar os Slides do workshop **Aplicando segurança em seu CI/CD com Gitlab e Horusec** ministrado por:

- [Thaynara Mendes]()
- [Samuel Gonçalves](https://beacons.ai/sgoncalves)

## whoami

### 🇧🇷 Samuel Gonçalves

* Tech Lead na 4Linux
* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻
* *"5k++"* alunos ensinados no 🌎
* Músico, Contista, YouTuber e Podcaster
* Apaixonado por Software Livre 💙

> Contato: [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

### 🏳️‍🌈 Thaynara Mendes

* Formada em Ciência da Computação
* Apaixonada por Linux e café
* Dev Python no tempo livre e atuo como Devops Engineer
* possuo a cerficação LPIC-1
* sou Integrante das Pyladies e ministro aula do curso de Infraestrutura Ágil com Práticas DevOps e Especialista em Elastic Stack na 4linux.

> Contato: [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

## Objetivos do Workshop

1. Compreender os princípios do DevSecOps
2. Implementar uma pipeline funcional no GitlabCI com SAST usando:
   1. GitlabCI
   2. Horusec
   3. Dependency Check
3. Listar ferramentas OpenSource para Segurança Ágil
4. Solucionar dúvidas relacionadas a Segurança no processo de desenvolvimento

## Laboratório

Link do repositório do laboratório: [https://gitlab.com/o_sgoncalves/lab-workshop-devsecops-horusec](https://gitlab.com/o_sgoncalves/lab-workshop-devsecops-horusec)
### Dependências

Para a criação do laboratório é necessário ter pré instalado os seguintes softwares:

* [Git][2]
* [VirtualBox][3]
* [Vagrant][5]

> Para o máquinas com Windows aconselhamos o uso do proprio Powershell e que as instalações dos softwares são feitas da maneira tradicional

> Para as máquinas com MAC OS aconselhamos, se possível, que as instalações sejam feitas pelo gerenciador de pacotes **brew**.

### Descrição do Ambiente

O Laboratório será criado utilizando o [Vagrant][7]. Ferramenta para criar e gerenciar ambientes virtualizados (baseado em Inúmeros providers) com foco em automação.

Nesse laboratórios, que está centralizado no arquivo [Vagrantfile][8], serão criadas 2 maquinas com as seguintes características:

Nome       | vCPUs | Memoria RAM | IP            | S.O.           | Script de Provisionamento
---------- |:-----:|:-----------:|:-------------:|:---------------:| -----------------------------
app    | 1     | 2048MB      | 192.168.56.50 | almalinux/8     | [provisionamento/app.yaml](provisionamento/app.yaml)
devsecops | 1     | 2048MB      | 192.168.56.60 | almalinux/8     | [provisionamento/devsecops.yaml](provisionamento/devsecops.yaml)

### Criação do Laboratório

Para criar o laboratório é necessário fazer o `git clone` desse repositório e, dentro da pasta baixada realizar a execução do `vagrant up`, conforme abaixo:

```bash
git clone https://gitlab.com/o_sgoncalves/lab-workshop-devsecops-horusec
cd lab-workshop-devsecops-horusec/
vagrant up
```

_O Laboratório **pode demorar**, dependendo da conexão de internet e poder computacional, para ficar totalmente preparado._

### Dicas de utilização

Por fim, para melhor utilização, abaixo há alguns comandos básicos do vagrant para gerencia das máquinas virtuais.

Comandos                | Descrição
:----------------------:| ---------------------------------------
`vagrant init`          | Gera o VagrantFile
`vagrant box add <box>` | Baixar imagem do sistema
`vagrant box status`    | Verificar o status dos boxes criados
`vagrant up`            | Cria/Liga as VMs baseado no VagrantFile
`vagrant provision`     | Provisiona mudanças logicas nas VMs
`vagrant status`        | Verifica se VM estão ativas ou não.
`vagrant ssh <vm>`      | Acessa a VM
`vagrant ssh <vm> -c <comando>` | Executa comando via ssh
`vagrant reload <vm>`   | Reinicia a VM
`vagrant halt`          | Desliga as VMs

> Para maiores informações acesse a [Documentação do Vagrant][13]

## Repositório da Aplicação

O repositório da aplicação com todos os arquivos configurados está disponível no link: [https://gitlab.com/thaycafe/coffee-shop](https://gitlab.com/thaycafe/coffee-shop)

## Horusec

Para começar a configurar o Horusec vamos acessar a máquina `devsecops` com o comando:
```shell=
vagrant ssh devsecops
```

Eleve seus privilégios aos do usuário `root` com o comando:
```shell=
sudo -i
```

Acesse o diretório `/opt/horusec` e realize a instalação com os comandos:
```shell=
cd /opt/horusec
make install
```

O provisionamento irá levar alguns minutos, é normal.

As credenciais padrão são:
```text=
email: dev@example.com
password: Devpass0*
```

Criar chave de API para projeto.

## Coffe Shop

Repositório da loja de cafés: [https://github.com/thaycafe/coffee-shop](https://github.com/thaycafe/coffee-shop)

[1]: https://sgoncalves.tec.br
[2]: https://git-scm.com/downloads
[3]: https://www.virtualbox.org/wiki/Downloads
[5]: https://www.vagrantup.com/downloads
[6]: https://cygwin.com/install.html
[7]: https://www.vagrantup.com/
[8]: ./Vagrantfile
[9]: ./provisionamento/app.sh
[10]: ./provisionamento/devsecops.sh
[11]: ./provisionamento/logging.sh
[12]: ./provisionamento/validation.sh
[13]: https://www.vagrantup.com/docs
[14]: https://app.vagrantup.com/4linux