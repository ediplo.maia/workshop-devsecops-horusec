---
marp: true
theme: uncover
paginate: true
backgroundColor: #141414
color: #fff
colorSecondary: #10b5bf
# backgroundImage: url('images/main-background.png')
style: |
    section{
      font-family: "Helvetica", monospace;
    }
    section::after {
      font-weight: bold;
      content: attr(data-marpit-pagination) '/' attr(data-marpit-pagination-total);
      font-size: 13pt;
      color: #10b5bf;
      text-shadow: 1px 1px 0 #000;
    }    
---

<style scoped>
  h2 {
    font-size: 40pt;
    list-style-type: circle;
    font-weight: 900;
    color: #fff
  }
  p {
    font-size: 20pt;
    font-weight: bold;
    list-style-type: circle;
    font-weight: 500;
    color: #10b5bf
  }
</style>

<!-- _paginate: false -->

# **DevSecOps**
### Aplicando segurança em seu CI/CD com Gitlab
Thaynara Mendes
Samuel Gonçalves

---

<!-- _paginate: false -->
> ## *A educação é a arma mais poderosa que você pode usar para mudar o mundo.*

*Nelson Mandela*

---
<style scoped>
  h4 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 900;
    color: #10b5bf
  }
  p {
    font-size: 13pt;
  }
  {
   font-size: 28px;
  }
  img[alt~="center"] {
    display: block;
    margin: 0 auto;
  }

</style>

<!-- _paginate: false -->

![bg right:40%](images/perfil.png)

#### 🇧🇷 Samuel Gonçalves

* Tech Lead na 4Linux
* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻
* *"5k++"* alunos ensinados no 🌎
* Músico, Contista, YouTuber e Podcaster
* Apaixonado por Software Livre 💙
* Contato: [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---
<style scoped>
  h4 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 900;
    color: #10b5bf
  }
  p {
    font-size: 13pt;
  }
  {
   font-size: 28px;
  }
  img[alt~="center"] {
    display: block;
    margin: 0 auto;
  }

</style>

<!-- _paginate: false -->

![bg left:40%](images/perfil-thay.png)

#### 🏳️‍🌈 Thaynara Mendes

* Formada em Ciência da Computação
* Apaixonada por Linux e café
* Dev Python no tempo livre e atuo como Devops Engineer
* possuo a cerficação LPIC-1
* sou Integrante das Pyladies e ministro aula do curso de Infraestrutura Ágil com Práticas DevOps e Especialista em Elastic Stack na 4linux.
* Contato: [https://www.linkedin.com/in/thaynaramendss/](https://www.linkedin.com/in/thaynaramendss/)

---

<style scoped>
  h3 {
    font-size: 40pt;
    list-style-type: circle;
    font-weight: 900;
    color: #fff
  }
  p {
    font-size: 20pt;
  }
  {
   font-size: 35px;
  }
</style>

### CONCORRA A UMA ASSINATURA DA 4LINUX!

![bg right:40% 80%](images/qr-sorteio.jpg)
#### É só escanear e se inscrever!

---

<style scoped>
  p {
  font-size: 25pt;
  list-style-type: circle;
}
</style>
![bg right:40% 80%](images/qrcode-slides.jpg)
### Esses slides são OpenSource! 
Escaneie e acesse!

---

## Segurança no desenvolvimento
![bg left:20%](images/developer.png)


É essencial trazer a segurança para o projeto desde o início do processo, garantindo maior proteção contra possíveis ameaças.

---


<style scoped>
  h2 {
    font-size: 50pt;
    list-style-type: circle;
    text-shadow: 3px 3px 8px #082a44;
    font-weight: 1000;
    color: #fff
  }
  p {
    font-size: 30pt;
  }
  {
   font-size: 35px;
  }
</style>

<!-- _paginate: false -->

## Dev**Sec**Ops

![w:900px](images/devsecops-banner.png)


---

<style scoped>
  h2 {
    font-size: 45pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 20pt;
    text-align: left;
  }
  {
   font-size: 25px;
   text-align: left;
  }
</style>

## Fundamentos **DevSecOps**

* Integração da segurança no ciclo de vida do desenvolvimento de software.
* Colaboração entre desenvolvedores, operações e profissionais de segurança.
* Automação de testes de segurança e correção de problemas.
* Cultura de segurança e conscientização de toda a equipe.

---

<style scoped>
  h2 {
    font-size: 60pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #fff;
    text-align: center;
  }

</style>

![bg right:40% 100%](images/devsecops-automation.png)

## Em **DevSecOps** a segurança é **Automatizada**
---

<!-- _paginate: false -->
![bg w:1300px](images/cicd.png)

---

<style scoped>
  h2 {
    font-size: 45pt;
    list-style-type: circle;
    font-weight: 900;
    color: #ffff;
    text-align: center;
  }
  p {
    font-size: 20pt;
    text-align: center;
  }
  {
   font-size: 25px;
   text-align: left;
  }
</style>

## Gitlab CI/CD
![w:500px ](images/gitlab_ci.png)

* Oferece funcionalidades como:
  * controle de versão de código-fonte
  * gerenciamento de problemas e revisão de código
  * integração com fluxos de trabalho ágeis e colaboração em tempo real
* É uma plataforma completa de CI/CD que permite a integração contínua e entrega contínua de software



---
<!-- _paginate: false -->
![bg w:1300px](images/ssdlm.png)


---

<style scoped>
table {
    height: 50%;
    width: 80%;
    font-size: 23px;

}
th {

}
h3 {
  font-size: 40pt;
  list-style-type: circle;
  font-weight: 900;
  color: #0ff;
  text-align: center;
}
p {
  font-size: 20pt;
  text-align: left;
}
{
 font-size: 25px;
 text-align: left;
}
a {
  color: #fff;
}

</style>

### Ferramentas para SAST

Ferramenta        | Linguagem               | Link para acesso
:---------------: |:-----------------------:|:----------------:
Brakeman          | Ruby                    | [https://brakemanscanner.org/](https://brakemanscanner.org/)
SonarQube         | 27 linguagens           | [https://www.sonarqube.org/](https://www.sonarqube.org/)
Bandit            | Python                  | [https://pypi.org/project/bandit/](https://pypi.org/project/bandit/)
Horusec           | Várias linguagens       | [https://horusec.io/](https://horusec.io/site/)
CPPCheck          | C++                     | [http://cppcheck.sourceforge.net/](http://cppcheck.sourceforge.net/)
Graudit           | Groovy                  | [https://github.com/wireghoul/graudit](https://github.com/wireghoul/graudit)
Dependency Check (SCA) | Dependências de Código  | [https://owasp.org/www-project-dependency-check/](https://owasp.org/www-project-dependency-check/)

---

<style scoped>
table {
    height: 50%;
    width: 80%;
    font-size: 23px;

}
th {

}
h3 {
  font-size: 40pt;
  list-style-type: circle;
  font-weight: 900;
  color: #0ff;
  text-align: center;
}
p {
  font-size: 20pt;
  text-align: left;
}
{
 font-size: 25px;
 text-align: left;
}
a {
  color: #fff;
}

</style>

### Ferramentas para DAST

Ferramenta       | Link para acesso
:---------------:|:----------------:
OWASP ZAP        | [https://owasp.org/www-project-zap/](https://owasp.org/www-project-zap/)
Arachni          | [https://www.arachni-scanner.com/](https://www.arachni-scanner.com/)
SQLmap           | [http://sqlmap.org/](http://sqlmap.org/)
Gauntlt          | [http://gauntlt.org/](http://gauntlt.org)
BDD Security     | [https://github.com/iriusrisk/bdd-security](https://github.com/iriusrisk/bdd-security)
Nikto            | [https://github.com/sullo/nikto](https://github.com/sullo/nikto)
Golismero        | [https://github.com/golismero/golismero](https://github.com/golismero/golismero)

---

<!-- _paginate: false -->
# Dúvidas?

![bg left:45% 100%](imagem/../images/duvida.png)

---

# Mãos a obra!

---
## Acesse o repositório do Coffee Shop:

### https://gitlab.com/thaycafe/coffee-shop

---

<style scoped>
h3 {
  font-size: 40pt;
  list-style-type: circle;
  font-weight: 900;
  color: #0ff;
  text-align: center;
}
p {
  font-size: 25pt;
}
{
 font-size: 25px;
}
a {
  color: #fff;
}
</style>



### Obrigado!

Vamos nos conectar?
* **Thaynara Mendes**: [https://www.linkedin.com/in/thaynaramendss/](https://www.linkedin.com/in/thaynaramendss/)
* **Samuel Gonçalves:** [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)


---
### Fontes Bibliográficas

<style scoped>
  p {
  font-size: 19pt;
  list-style-type: circle;
}
a {
  color: #fff;
}
</style>

[https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/](https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/)
[https://www.redhat.com/pt-br/topics/devops/what-is-devsecops](https://www.redhat.com/pt-br/topics/devops/what-is-devsecops)
[https://www.ibm.com/br-pt/cloud/learn/devsecops](https://www.ibm.com/br-pt/cloud/learn/devsecops)
[https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/](https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/)
[https://4linux.com.br/cursos/treinamento/devsecops-seguranca-em-infraestrutura-e-desenvolvimento-agil/](https://4linux.com.br/cursos/treinamento/devsecops-seguranca-em-infraestrutura-e-desenvolvimento-agil/)
[https://blog.4linux.com.br/devsecops-implementacao-em-6-passos/](https://blog.4linux.com.br/devsecops-implementacao-em-6-passos/)
[https://blog.gitguardian.com/security-tools-shift-left/](https://blog.gitguardian.com/security-tools-shift-left/)
[https://michelleamesquita.medium.com/entendendo-o-ciclo-de-vida-de-desenvolvimento-de-software-seguro-ssdlc-ccc173f583de](https://michelleamesquita.medium.com/entendendo-o-ciclo-de-vida-de-desenvolvimento-de-software-seguro-ssdlc-ccc173f583de)